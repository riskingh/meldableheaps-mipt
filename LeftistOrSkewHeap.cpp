//
//  LeftistOrSkewHeap.cpp
//  MeldableHeaps
//
//  Created by Максим Гришкин on 17/12/14.
//  Copyright (c) 2014 Максим Гришкин. All rights reserved.
//

#include "LeftistOrSkewHeap.h"

namespace NHeaps {
    CLeftistOrSkewHeap::CLeftistOrSkewHeap()
    : rank(0), left(NULL), right(NULL) {}
    
    CLeftistOrSkewHeap::CLeftistOrSkewHeap(bool _isLeftist)
    : isLeftist(_isLeftist), rank(0), left(NULL), right(NULL) {}
    
    CLeftistOrSkewHeap::CLeftistOrSkewHeap(bool _isLeftist, int _key)
    : isLeftist(_isLeftist), rank(1), key(_key), left(NULL), right(NULL) {}
    
    CLeftistOrSkewHeap::~CLeftistOrSkewHeap() {
        delete left;
        delete right;
    }
    
    void CLeftistOrSkewHeap::swapWith(CLeftistOrSkewHeap *_other) {
        std::swap(isLeftist, _other->isLeftist);
        std::swap(rank, _other->rank);
        std::swap(key, _other->key);
        std::swap(left, _other->left);
        std::swap(right, _other->right);
    }
    
    int getRank(CLeftistOrSkewHeap *_heap) {
        if (!_heap)
            return 0;
        return _heap->rank;
    }
    
    void CLeftistOrSkewHeap::updateRank() {
        rank = 1 + std::min(getRank(left), getRank(right));
    }
    
    void CLeftistOrSkewHeap::meld(IMeldableHeap *_other) {
        CLeftistOrSkewHeap *other = dynamic_cast<CLeftistOrSkewHeap *>(_other);
        if (!other || other->rank == 0)
            return;
        if (rank == 0) {
            *this = *other;
            other->left = other->right = NULL;
            delete other;
            return;
        }
        if (key > other->key)
            swapWith(other);
        
        if (right)
            right->meld(other);
        else right = other;
        
        if (!isLeftist || getRank(left) < getRank(right))
            std::swap(left, right);
        updateRank();
    }
    
    void CLeftistOrSkewHeap::insert(int _key) {
        CLeftistOrSkewHeap *newHeap = new CLeftistOrSkewHeap(isLeftist, _key);
        meld(newHeap);
    }
    
    int CLeftistOrSkewHeap::getMin() const {
        if (!rank)
            return INT32_MAX;
        return key;
    }
    
    int CLeftistOrSkewHeap::extractMin() {
        if (!rank)
            return INT32_MAX;
        int result = key;
        if (right)
            std::swap(left, right);
        if (left) {
            CLeftistOrSkewHeap *tempLeft = left, *tempRight = right;
            *this = *left;
            tempLeft->left = tempLeft->right = NULL;
            delete tempLeft;
            meld(tempRight);
        }
        else
            rank = 0;
        return result;
    }
}