//
//  SkewHeap.h
//  MeldableHeaps
//
//  Created by Максим Гришкин on 15/12/14.
//  Copyright (c) 2014 Максим Гришкин. All rights reserved.
//

#ifndef __MeldableHeaps__SkewHeap__
#define __MeldableHeaps__SkewHeap__

#include <stdio.h>
#include <algorithm>

#include "LeftistOrSkewHeap.h"

namespace NHeaps {
    class CSkewHeap: public CLeftistOrSkewHeap {
    public:
        CSkewHeap();
        explicit CSkewHeap(int);
    };
}

#endif /* defined(__MeldableHeaps__SkewHeap__) */
