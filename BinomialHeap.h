//
//  BinomialHeap.h
//  MeldableHeaps
//
//  Created by Максим Гришкин on 15/12/14.
//  Copyright (c) 2014 Максим Гришкин. All rights reserved.
//

#ifndef __MeldableHeaps__BinomialHeap__
#define __MeldableHeaps__BinomialHeap__

#include <stdio.h>
#include <iostream>
#include <algorithm>

#include "MeldableHeap.h"

namespace NHeaps {
    class CBinomialHeap: public IMeldableHeap {
    private:
        CBinomialHeap *leftChild, *rightBrother;
        int degree, key;
        
        void meldBinomialTreeWithNext();
        
    public:
        CBinomialHeap();
        explicit CBinomialHeap(int);
        ~CBinomialHeap();
        void deleteOne();
        
        void swapWith(CBinomialHeap *);
        
        void meld(IMeldableHeap *);
        void insert(int);
        int getMin() const;
        int extractMin();
        
        void showDegrees() const;
        
        friend void pushBackAndMove(CBinomialHeap *&, CBinomialHeap *&);
        friend void mergeBinomialTreeLists(CBinomialHeap *, CBinomialHeap *);
        friend CBinomialHeap *reversed(CBinomialHeap *);
    };
}

#endif /* defined(__MeldableHeaps__BinomialHeap__) */
