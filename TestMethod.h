//
//  TestMethod.h
//  MeldableHeaps
//
//  Created by Максим Гришкин on 16/12/14.
//  Copyright (c) 2014 Максим Гришкин. All rights reserved.
//

#ifndef MeldableHeaps_TestMethod_h
#define MeldableHeaps_TestMethod_h

#include <iostream>

namespace NTests {
    enum EMethod {
        M_ADD_HEAP,
        M_INSERT,
        M_EXTRACT_MIN,
        M_MELD,
        M_NUMBER
    };
    
    struct CTestMethod {
        EMethod type;
        unsigned int firstIndex, secondIndex;
        int key;
        CTestMethod (EMethod _type)
        : type(_type), firstIndex(0), secondIndex(0), key(0) {}
        
        void show() const {
            switch (type) {
                case (M_ADD_HEAP):
                    std::cout << "AddHeap";
                    break;
                case (M_INSERT):
                    std::cout << "Insert";
                    break;
                case (M_EXTRACT_MIN):
                    std::cout << "ExtractMin";
                    break;
                case (M_MELD):
                    std::cout << "Meld";
                    break;
                default:
                    break;
            }
            std::cout << " {i1: " << firstIndex << ", i2: " << secondIndex << ", key: " << key << "} ";
        }
    };
}

#endif
