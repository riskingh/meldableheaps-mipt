//
//  LeftistOrSkewHeap.h
//  MeldableHeaps
//
//  Created by Максим Гришкин on 17/12/14.
//  Copyright (c) 2014 Максим Гришкин. All rights reserved.
//

#ifndef __MeldableHeaps__LeftistOrSkewHeap__
#define __MeldableHeaps__LeftistOrSkewHeap__

#include <stdio.h>
#include <algorithm>

#include "MeldableHeap.h"

namespace NHeaps {
    
    class CLeftistOrSkewHeap: public IMeldableHeap {
    private:
        bool isLeftist;
        int rank, key;
        CLeftistOrSkewHeap *left, *right;
    public:
        CLeftistOrSkewHeap();
        explicit CLeftistOrSkewHeap(bool);
        CLeftistOrSkewHeap(bool, int);
        virtual ~CLeftistOrSkewHeap();
        
        void swapWith(CLeftistOrSkewHeap *);
        
        friend int getRank(CLeftistOrSkewHeap *);
        void updateRank();
        
        void meld(IMeldableHeap *);
        void insert(int);
        int getMin() const;
        int extractMin();
    };
    
}

#endif /* defined(__MeldableHeaps__LeftistOrSkewHeap__) */
