//
//  LeftistHeap.cpp
//  MeldableHeaps
//
//  Created by Максим Гришкин on 14/12/14.
//  Copyright (c) 2014 Максим Гришкин. All rights reserved.
//

#include "LeftistHeap.h"

namespace NHeaps {
    CLeftistHeap::CLeftistHeap()
    : CLeftistOrSkewHeap(true) {}
    
    CLeftistHeap::CLeftistHeap(int _key)
    : CLeftistOrSkewHeap(true, _key) {}
    
}