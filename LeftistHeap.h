//
//  LeftistHeap.h
//  MeldableHeaps
//
//  Created by Максим Гришкин on 14/12/14.
//  Copyright (c) 2014 Максим Гришкин. All rights reserved.
//

#ifndef __MeldableHeaps__LeftistHeap__
#define __MeldableHeaps__LeftistHeap__

#include <stdio.h>
#include <algorithm>
#include <iostream>

#include "LeftistOrSkewHeap.h"

namespace NHeaps {
    class CLeftistHeap: public CLeftistOrSkewHeap {
    public:
        CLeftistHeap();
        explicit CLeftistHeap(int);
    };
}

#endif /* defined(__MeldableHeaps__LeftistHeap__) */
