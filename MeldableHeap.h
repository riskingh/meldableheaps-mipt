//
//  MeldableHeap.h
//  MeldableHeaps
//
//  Created by Максим Гришкин on 08/12/14.
//  Copyright (c) 2014 Максим Гришкин. All rights reserved.
//

#ifndef MeldableHeaps_MeldableHeap_h
#define MeldableHeaps_MeldableHeap_h

namespace NHeaps {
    class IMeldableHeap {
    private:
    public:
        IMeldableHeap() {}
        
        virtual void meld(IMeldableHeap *) = 0;
        virtual void insert(int) = 0;
        virtual int getMin() const = 0;
        virtual int extractMin() = 0;
        virtual ~IMeldableHeap() {}
    };
}

#endif
