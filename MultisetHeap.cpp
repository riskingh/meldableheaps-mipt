//
//  MultisetHeap.cpp
//  MeldableHeaps
//
//  Created by Максим Гришкин on 17/12/14.
//  Copyright (c) 2014 Максим Гришкин. All rights reserved.
//

#include "MultisetHeap.h"

namespace NHeaps {
    CMultisetHeap::CMultisetHeap() {}
    
    CMultisetHeap::CMultisetHeap(int _key) {
        data.insert(_key);
    }
    
    CMultisetHeap::~CMultisetHeap() {
    }
    
    void CMultisetHeap::meld(IMeldableHeap *_other) {
        CMultisetHeap *other = dynamic_cast<CMultisetHeap *>(_other);
        for (const auto &key: other->data)
            data.insert(key);
    }
    
    void CMultisetHeap::insert(int _key) {
        data.insert(_key);
    }
    
    int CMultisetHeap::getMin() const {
        return *data.begin();
    }
    
    int CMultisetHeap::extractMin() {
        if (data.empty())
            return INT32_MAX;
        int minimum = *data.begin();
        data.erase(data.begin());
        return minimum;
    }
}