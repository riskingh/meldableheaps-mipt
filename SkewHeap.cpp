//
//  SkewHeap.cpp
//  MeldableHeaps
//
//  Created by Максим Гришкин on 15/12/14.
//  Copyright (c) 2014 Максим Гришкин. All rights reserved.
//

#include "SkewHeap.h"

namespace NHeaps {
    CSkewHeap::CSkewHeap()
    : CLeftistOrSkewHeap(false) {}
    
    CSkewHeap::CSkewHeap(int _key)
    : CLeftistOrSkewHeap(false, _key) {}
}