//
//  main.cpp
//  MeldableHeaps
//
//  Created by Максим Гришкин on 08/12/14.
//  Copyright (c) 2014 Максим Гришкин. All rights reserved.
//

#include <iostream>

#include "LeftistHeap.h"
#include "SkewHeap.h"
#include "BinomialHeap.h"

#include "Tests.h"

#include <string>
#include <cmath>

double tableRound(double number) {
    return (double)round(number * 10000) / 10000.0;
}

int main(int argc, const char * argv[]) {
//    testEquality(int _testsNumber, int _addHeapMethodsNumber, int _insertMethodsNumber, int _extractMinMethodsNumber, int _meldMethodsNumber)
//    testTime(int _testsNumber, int _addHeapMethodsNumber, int _insertMethodsNumber, int _extractMinMethodsNumber, int _meldMethodsNumber)
    
    unsigned int testsNumber, addHeapMethodsNumber, insertMethodsNumber, extractMinMethodsNumber, meldMethodsNumber;
    
    std::string testType;
    std::cout << "Test types:\nequality - compares results of Leftist, Skew, Binomial, Multiset heaps\ntime - measures time\ntable - creates table\n";
    std::cin >> testType;
    if (testType == "equality") {
        std::cout << "testEquality(ui _testsNumber, ui _addHeapMethodsNumber, ui _insertMethodsNumber, ui _extractMinMethodsNumber, ui _meldMethodsNumber)\n";
        std::cin >> testsNumber >> addHeapMethodsNumber >> insertMethodsNumber >> extractMinMethodsNumber >> meldMethodsNumber;
        NTests::testEquality(testsNumber, addHeapMethodsNumber, insertMethodsNumber, extractMinMethodsNumber, meldMethodsNumber);
    }
    else if (testType == "time") {
        std::cout << "testTime(ui _testsNumber, ui _addHeapMethodsNumber, ui _insertMethodsNumber, ui _extractMinMethodsNumber, ui _meldMethodsNumber)\n";
        std::cin >> testsNumber >> addHeapMethodsNumber >> insertMethodsNumber >> extractMinMethodsNumber >> meldMethodsNumber;
        NTests::testTime(testsNumber, addHeapMethodsNumber, insertMethodsNumber, extractMinMethodsNumber, meldMethodsNumber);
    }
    else if (testType == "table") {
        std::vector<unsigned int> testsNumbers, heapsNumbers, queries;
        size_t test;
        testsNumbers    = {100, 100,    10,     10,     10};
        heapsNumbers    = {25,  250,    2500,   25000,  250000};
        queries         = {25,  250,    2500,   25000,  250000};
        std::vector<std::vector<double>> times(testsNumbers.size(), std::vector<double>(3, 1.2));
        for (test = 0; test < testsNumbers.size(); ++test) {
            times[test] = NTests::testTime(testsNumbers[test], heapsNumbers[test], queries[test], queries[test], heapsNumbers[test] - 5, false);
            std::cout << "done\n";
        }
        for (test = 0; test < testsNumbers.size(); ++test) {
            for (int heapType = 0; heapType < NTests::HT_NUMBER; ++heapType) {
                times[test][heapType] = tableRound(times[test][heapType]);
            }
        }
        std::cout << "| * TABLE * | ";
        for (test = 0; test < testsNumbers.size(); ++test)
            std::cout << queries[test] << " | ";
        std::cout << "\n|:---:|";
        for (test = 0; test < testsNumbers.size(); ++test)
            std::cout << ":---:|";
        std::cout << "\n";
        std::cout << "| Leftist | ";
        for (test = 0; test < testsNumbers.size(); ++test)
            std::cout << times[test][NTests::HT_LEFTIST] << " | ";
        std::cout << "\n";
        std::cout << "| Skew | ";
        for (test = 0; test < testsNumbers.size(); ++test)
            std::cout << times[test][NTests::HT_SKEW] << " | ";
        std::cout << "\n";
        std::cout << "| Binomial | ";
        for (test = 0; test < testsNumbers.size(); ++test)
            std::cout << times[test][NTests::HT_BINOMIAL] << " | ";
        std::cout << "\n";
        std::cout << "| Multiset | ";
        for (test = 0; test < testsNumbers.size(); ++test)
            std::cout << times[test][NTests::HT_MULTISET] << " | ";
        std::cout << "\n";

    }
    else {
    }
    
    return 0;
}
