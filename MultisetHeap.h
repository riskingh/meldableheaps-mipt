//
//  MultisetHeap.h
//  MeldableHeaps
//
//  Created by Максим Гришкин on 17/12/14.
//  Copyright (c) 2014 Максим Гришкин. All rights reserved.
//

#ifndef __MeldableHeaps__MultisetHeap__
#define __MeldableHeaps__MultisetHeap__

#include <stdio.h>
#include <set>

#include "MeldableHeap.h"

namespace NHeaps {
    class CMultisetHeap: public IMeldableHeap {
    private:
        std::multiset<int> data;
    public:
        CMultisetHeap();
        CMultisetHeap(int _key);
        ~CMultisetHeap();
        
        void meld(IMeldableHeap *);
        void insert(int);
        int getMin() const;
        int extractMin();
    };
}

#endif /* defined(__MeldableHeaps__MultisetHeap__) */
