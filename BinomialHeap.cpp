//
//  BinomialHeap.cpp
//  MeldableHeaps
//
//  Created by Максим Гришкин on 15/12/14.
//  Copyright (c) 2014 Максим Гришкин. All rights reserved.
//

#include "BinomialHeap.h"

namespace NHeaps {
    CBinomialHeap::CBinomialHeap()
    : IMeldableHeap(), leftChild(NULL), rightBrother(NULL), degree(-1) {}
    
    CBinomialHeap::CBinomialHeap(int _key)
    : IMeldableHeap(), leftChild(NULL), rightBrother(NULL), degree(0), key(_key) {}
    
    CBinomialHeap::~CBinomialHeap() {
        delete leftChild;
        delete rightBrother;
    }
    
    void CBinomialHeap::deleteOne() {
        leftChild = rightBrother = NULL;
        delete this;
    }
    
    
    void CBinomialHeap::swapWith(CBinomialHeap *_other) {
        std::swap(leftChild, _other->leftChild);
        std::swap(rightBrother, _other->rightBrother);
        std::swap(degree, _other->degree);
        std::swap(key, _other->key);
    }
    
    void pushBackAndMove(CBinomialHeap *&_listEnd, CBinomialHeap *&_newElement) {
        _listEnd->rightBrother = _newElement;
        _listEnd = _listEnd->rightBrother;
        _newElement = _newElement->rightBrother;
    }
    
    void mergeBinomialTreeLists(CBinomialHeap *_first, CBinomialHeap *_second) {
        if (_first->degree > _second->degree)
            _first->swapWith(_second);
        CBinomialHeap *listLast, *curSecond, *temporary;
        for (listLast = _first, curSecond = _second; listLast->rightBrother && curSecond;) {
            if (listLast->rightBrother->degree <= curSecond->degree)
                listLast = listLast->rightBrother;
            else {
                temporary = listLast->rightBrother;
                listLast->rightBrother = curSecond;
                curSecond = curSecond->rightBrother;
                listLast = listLast->rightBrother;
                listLast->rightBrother = temporary;
            }
        }
        if (curSecond)
            listLast->rightBrother = curSecond;
    }
    
    void CBinomialHeap::meldBinomialTreeWithNext() {
        if (key > rightBrother->key) {
            std::swap(key, rightBrother->key);
            std::swap(leftChild, rightBrother->leftChild);
        }
        CBinomialHeap *nextBinomialTree = rightBrother;
        rightBrother = rightBrother->rightBrother;
        nextBinomialTree->rightBrother = leftChild;
        leftChild = nextBinomialTree;
        ++degree;
    }
    
    void CBinomialHeap::meld(IMeldableHeap *_other) {
        CBinomialHeap *other = dynamic_cast<CBinomialHeap *>(_other);
        if (!other || other->degree == -1)
            return;
        if (degree == -1) {
            swapWith(other);
            other->deleteOne();
            return;
        }
        
        mergeBinomialTreeLists(this, other);
        
        CBinomialHeap *currentBinomialTree = this;
        
        while (currentBinomialTree && currentBinomialTree->rightBrother) {
            if (currentBinomialTree->degree == currentBinomialTree->rightBrother->degree)
                currentBinomialTree->meldBinomialTreeWithNext();
            else
                currentBinomialTree = currentBinomialTree->rightBrother;
        }
    }
    
    void CBinomialHeap::insert(int _key) {
        meld(new CBinomialHeap(_key));
    }
    
    int CBinomialHeap::getMin() const {
        if (degree == -1)
            return INT32_MAX;
        int minimum = key;
        for (CBinomialHeap *currentBinomialTree = this->rightBrother;
             currentBinomialTree;
             currentBinomialTree = currentBinomialTree->rightBrother)
            minimum = std::min(minimum, currentBinomialTree->key);
        return minimum;
    }
    
    
    CBinomialHeap *reversed(CBinomialHeap *_firstBinomialTree) {
        CBinomialHeap *curBinomialTree = _firstBinomialTree, *prevBinomialTree = NULL, *temporary;
        while (curBinomialTree) {
            temporary = curBinomialTree->rightBrother;
            curBinomialTree->rightBrother = prevBinomialTree;
            prevBinomialTree = curBinomialTree;
            curBinomialTree = temporary;
        }
        return prevBinomialTree;
    }
    
    int CBinomialHeap::extractMin() {
        if (degree == -1)
            return INT32_MAX;
        int minimum = getMin();
        CBinomialHeap *minBinomialTree;
        if (key == minimum) {
            minBinomialTree = this->leftChild;
            if (this->rightBrother)
                *this = *this->rightBrother;
            else {
                degree = -1;
                leftChild = NULL;
            }
        }
        else {
            CBinomialHeap *prevBinomialTree = this, *curBinomialTree = this->rightBrother;
            while (curBinomialTree->key != minimum) {
                prevBinomialTree = curBinomialTree;
                curBinomialTree = curBinomialTree->rightBrother;
            }
            prevBinomialTree->rightBrother = curBinomialTree->rightBrother;
            minBinomialTree = curBinomialTree->leftChild;
        }
        meld(reversed(minBinomialTree));
        return minimum;
    }
    
    void CBinomialHeap::showDegrees() const {
        const CBinomialHeap *current = this;
        while (current) {
            std::cout << current->degree << " ";
            current = current->rightBrother;
        }
        std::cout << "\n";
    }
}