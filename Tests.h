//
//  Tests.h
//  MeldableHeaps
//
//  Created by Максим Гришкин on 16/12/14.
//  Copyright (c) 2014 Максим Гришкин. All rights reserved.
//

#ifndef MeldableHeaps_Tests_h
#define MeldableHeaps_Tests_h

#include "LeftistHeap.h"
#include "SkewHeap.h"
#include "LeftistOrSkewHeap.h"
#include "BinomialHeap.h"
#include "MultisetHeap.h"

#include "TestMethod.h"

#include <vector>
#include <algorithm>
#include <random>
#include <ctime>
#include <cassert>

namespace NTests {
    enum EHeapType {
        HT_LEFTIST,
        HT_SKEW,
        HT_BINOMIAL,
        HT_MULTISET,
        HT_NUMBER
    };
    
    NHeaps::IMeldableHeap *createHeapOfType(EHeapType _type, int _key) {
        NHeaps::IMeldableHeap *result = NULL;
        switch (_type) {
            case HT_LEFTIST:
                result = new NHeaps::CLeftistHeap(_key);
                break;
            case HT_SKEW:
                result = new NHeaps::CSkewHeap(_key);
                break;
            case HT_BINOMIAL:
                result = new NHeaps::CBinomialHeap(_key);
                break;
            case HT_MULTISET:
                result = new NHeaps::CMultisetHeap(_key);
                break;
            default:
                break;
        }
        return result;
    }
    
    std::default_random_engine mainGenerator((unsigned int)time(0));
//    std::default_random_engine mainGenerator;
    int getRandomInt(int _min, int _max) {
        std::uniform_int_distribution<int> distribution(_min, _max);
        return distribution(mainGenerator);
    }
    
    void pushBackMethods(std::vector<CTestMethod *> &_vector, unsigned int _number, EMethod _type) {
        for (; _number > 0; --_number)
            _vector.push_back(new CTestMethod(_type));
    }
    
    struct CTest {
        std::vector<CTestMethod *> methods;
        
        CTest() {
            std::cout << "test\n";
        }
        
        void generate(unsigned int _addHeapMethodsNumber, unsigned int _insertMethodsNumber, unsigned int _extractMinMethodsNumber, unsigned int _meldMethodsNumber) {
            size_t methodIndex;
            for (methodIndex = 0; methodIndex < methods.size(); ++methodIndex)
                delete methods[methodIndex];
            std::vector<CTestMethod *> temporary;
            std::swap(methods, temporary);
            std::vector<unsigned int> numbers = {_addHeapMethodsNumber, _insertMethodsNumber, _extractMinMethodsNumber, _meldMethodsNumber};
            methods.reserve(_addHeapMethodsNumber + _insertMethodsNumber + _extractMinMethodsNumber + _meldMethodsNumber);
            
            for (methodIndex = 0; methodIndex < M_NUMBER; ++methodIndex)
                pushBackMethods(methods, numbers[methodIndex], (EMethod)methodIndex);
            std::shuffle(methods.begin() + _addHeapMethodsNumber, methods.end(), std::default_random_engine((unsigned int)time(0)));
            
            int heaps = 0;
            for (auto &method: methods) {
                switch (method->type) {
                    case (M_ADD_HEAP):
                        method->key = getRandomInt(INT32_MIN, INT32_MAX);
                        ++heaps;
                        break;
                    case (M_INSERT):
                        method->firstIndex = getRandomInt(0, heaps - 1);
                        method->key = getRandomInt(INT32_MIN, INT32_MAX);
                        break;
                    case (M_EXTRACT_MIN):
                        method->firstIndex = getRandomInt(0, heaps - 1);
                        break;
                    case (M_MELD):
                        assert(heaps >= 2);
                        method->firstIndex = getRandomInt(0, heaps - 2);
                        method->secondIndex = getRandomInt(method->firstIndex + 1, heaps - 1);
                        --heaps;
                        break;
                    default:
                        break;
                }
            }
        }
        
        std::vector<int> run(EHeapType _heapType) const {
            std::vector<int> results;
            std::vector<NHeaps::IMeldableHeap *> heaps;
            for (const auto &method: methods) {
                switch (method->type) {
                    case (M_ADD_HEAP):
                        heaps.push_back(createHeapOfType(_heapType, method->key));
                        break;
                    case (M_INSERT):
                        heaps[method->firstIndex]->insert(method->key);
                        break;
                    case (M_EXTRACT_MIN):
                        results.push_back(heaps[method->firstIndex]->extractMin());
                        break;
                    case (M_MELD):
                        heaps[method->firstIndex]->meld(heaps[method->secondIndex]);
                        std::swap(heaps[method->secondIndex], heaps[heaps.size() - 1]);
                        heaps.pop_back();
                        break;
                    default:
                        break;
                }
            }
            for (size_t heapIndex = 0; heapIndex < heaps.size(); ++heapIndex)
                delete heaps[heapIndex];
            return results;
        }
    };
    
    bool equal(const std::vector<int> &_first, const std::vector<int> &_second) {
        if (_first.size() != _second.size())
            return false;
        for (size_t index = 0; index < _first.size(); ++index) {
            if (_first[index] != _second[index])
                return false;
        }
        return true;
    }
    
    std::vector<unsigned int> testEquality(unsigned int _testsNumber, unsigned int _addHeapMethodsNumber, unsigned int _insertMethodsNumber, unsigned int _extractMinMethodsNumber, unsigned int _meldMethodsNumber, bool _show = true) {
        CTest test;
        std::vector<int> results[HT_NUMBER];
        std::vector<unsigned int> correct(HT_NUMBER - 1, 0);
        size_t heapType;
        for (size_t testsNumber = 1; testsNumber <= _testsNumber; ++testsNumber) {
            test.generate(_addHeapMethodsNumber, _insertMethodsNumber, _extractMinMethodsNumber, _meldMethodsNumber);
            for (heapType = 0; heapType < HT_NUMBER; ++heapType)
                results[heapType] = test.run((EHeapType)heapType);
            for (heapType = 0; heapType + 1 < HT_NUMBER; ++heapType) {
                if (equal(results[heapType], results[HT_MULTISET]))
                    ++correct[heapType];
            }
        }
        if (_show) {
            std::cout << "   Tests: " << _testsNumber << "\n";
            std::cout << " Leftist: " << correct[HT_LEFTIST] << "\n";
            std::cout << "    Skew: " << correct[HT_SKEW] << "\n";
            std::cout << "Binomial: " << correct[HT_BINOMIAL] << "\n";
        }
        return correct;
    }
    
    std::vector<double> testTime(unsigned int _testsNumber, unsigned int _addHeapMethodsNumber, unsigned int _insertMethodsNumber, unsigned int _extractMinMethodsNumber, unsigned int _meldMethodsNumber, bool _show = true) {
        CTest *test = new CTest();
        std::vector<clock_t> totalTicks(HT_NUMBER, 0);
        clock_t begin, end;
        for (size_t testsNumber = 1; testsNumber <= _testsNumber; ++testsNumber) {
            test->generate(_addHeapMethodsNumber, _insertMethodsNumber, _extractMinMethodsNumber, _meldMethodsNumber);
            for (size_t heapType = 0; heapType < HT_NUMBER; ++heapType) {
                begin = clock();
                test->run((EHeapType)heapType);
                end = clock();
                totalTicks[heapType] += (end - begin);
            }
        }
        std::vector<double> times(HT_NUMBER);
        for (size_t heapType = 0; heapType < HT_NUMBER; ++heapType)
            times[heapType] = (double)totalTicks[heapType] / (double)_testsNumber / (double)CLOCKS_PER_SEC;
        if (_show) {
            std::cout << " Leftist: " << times[HT_LEFTIST] << "\n";
            std::cout << "    Skew: " << times[HT_SKEW] << "\n";
            std::cout << "Binomial: " << times[HT_BINOMIAL] << "\n";
            std::cout << "Multiset: " << times[HT_MULTISET] << "\n";
        }
        return times;
    }
};

#endif
